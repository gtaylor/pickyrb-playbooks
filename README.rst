Example Ruby Picky Playbooks
============================

This repository contains everything needed to provision a simple multi-server
Picky_ environment. Due to time constraints, this does not represent a
fully secured/optimized/monitored deployment. It is enough to provide
a reasonable start.

Quickstart
----------

First, make sure you have ansible installed::

    pip install ansible

You'll also need to grab Vagrant_. Once you have both of these, you can
provision a local test environment, consisting of two VMs::

    vagrant up

Production deploys
------------------

Due to time constraints, instance creation is not included in this set of
playbooks. So create your instances, then edit the ``ec2_hosts`` file. You'll
also want to edit ``group_vars/picky_prod_clients``, namely the
``picky_server_hostname`` (DNS this sucker). Then get the party started with::

    ansible-playbook -i ec2_hosts production.yml


License
-------

These playbooks are licensed under the BSD License. A copy is included in
the repo as ``LICENSE.txt``.

.. _Picky: http://pickyrb.com/
.. _Vagrant: https://www.vagrantup.com/
